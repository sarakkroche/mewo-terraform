# mewo-terraform



## Pipeline terraform pour Azure

Exemple de pipeline pour déployer l'infrastructure sur Azure.

Les fichiers Terraform pour Azure sont réutilisés pour automatiser les actions de :
- terraform validate
- terraform plan
- terraform apply
- terraform destroy

