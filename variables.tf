variable "prefix" {
  # A personnaliser avec votre nom
  default = "demo"
}

variable "environment" {
  default = "demo"
}

variable "project" {
  default = "MEWO"
}

variable "sshkey" {
  default = ""
}